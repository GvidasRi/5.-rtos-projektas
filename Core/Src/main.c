/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#include "string.h"
#include "stdio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint16_t timeron = 0;
uint16_t count = 0;
int test = 0;
//uint16_t value = 0;
uint16_t pwm_value = 0;
uint16_t duty = 0;
uint16_t step = 0;
uint8_t Rx_data;
uint8_t Rx_byte[];
uint8_t Rx_index = 0;
char LEDON[] = "LEDON";
char LEDOFF[] = "LEDOFF";
char BLINK[] = "BLINK";
char BUTTON[] = "BUTTON";
char PWM[] = "PWM";
char PULSE[] = "PULSE";
/* USER CODE END PV */


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM2_Init(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
xTaskHandle Receiver_Handler;

xQueueHandle SimpleQueue;

void Receiver_Task (void *argument);

uint16_t timer_val;
char str[100];
char str2[100];
char str3[100];
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
/*	LEDON[0] = 'L'; LEDON[1] = 'E'; LEDON[2] = 'D'; LEDON[3] = 'O'; LEDON[4] = 'N';
	LEDOFF[0] = 'L'; LEDOFF[1] = 'E'; LEDOFF[2] = 'D'; LEDOFF[3] = 'O'; LEDOFF[4] = 'F', LEDOFF[5] = 'F';
	BLINK[0] = 'B'; BLINK[1] = 'L'; BLINK[2] = 'I'; BLINK[3] = 'N'; BLINK[4] = 'K';
	BUTTON[0] = 'B'; BUTTON[1] = 'U'; BUTTON[2] = 'T'; BUTTON[3] = 'T'; BUTTON[4] = 'O', BUTTON[5] = 'N';
	PWM[0] = 'P'; PWM[1] = 'W'; PWM[2] = 'M';
	PULSE[0] = 'P'; PULSE[1] = 'U'; PULSE[2] = 'L'; PULSE[3] = 'S'; PULSE[4] = 'E';*/
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  timer_val = __HAL_TIM_GET_COUNTER(&htim2);

    SimpleQueue = xQueueCreate(5, sizeof (int));
      if (SimpleQueue == 0)  // Queue not created
      {
    	  char *str = "Unable to create Integer Queue\n\n";
    	  HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
      }
      else
      {
    	  char *str = "Komandos: LEDON, LEDOFF, BLINK, BUTTON, PWM, PULSE\n";
    	  HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
      }

      xTaskCreate(Receiver_Task, "Receive", 128, NULL, 1, &Receiver_Handler);

      HAL_UART_Receive_IT(&huart2, &Rx_data, 1);


      vTaskStartScheduler();

  /* USER CODE END 2 */

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 79;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : B1_button_Pin */
  GPIO_InitStruct.Pin = B1_button_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_button_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void user_pwm_setvalue_button(uint16_t value)
{
    TIM_OC_InitTypeDef sConfigOC;
    HAL_TIM_PWM_Init(&htim2);
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = value;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
}

void user_pwm_setvalue(uint16_t value)
{
	//HAL_TIM_Base_Stop_IT(&htim2);
    TIM_OC_InitTypeDef sConfigOC;
    HAL_TIM_PWM_Init(&htim2);
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = value;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
 //   HAL_TIM_Base_Start_IT(&htim2);
}

void Receiver_Task (void *argument)
{
	int received=0;
	uint32_t TickDelay = pdMS_TO_TICKS(500);
	while (1)
	{
		//strcpy (str, "Entered RECEIVER Task\n about to RECEIVE a number from the queue\n\n");
		//HAL_UART_Transmit(&huart2, (uint8_t *)str2, strlen (str2), HAL_MAX_DELAY);

		if (xQueueReceive(SimpleQueue, &received, portMAX_DELAY) != pdTRUE) //priima ToSend value ir ideda i recieved kintamaji (PT.2)
		{
			HAL_UART_Transmit(&huart2, (uint8_t *)"Error in Receiving from Queue\n\n", 31, 1000);
		}
		else
		{
			if (strcmp((const char *)Rx_byte, (const char *)LEDON) == 0)
			{
				HAL_TIM_Base_Stop_IT(&htim2); //sustabdo timer interrupta
				TIM_OC_InitTypeDef sConfigOC; //define sconfig
				HAL_TIM_PWM_Init(&htim2); //inicijuoja pwm (neveikia pulse keitimas be jo)
				sConfigOC.OCMode = TIM_OCMODE_PWM1;
				sConfigOC.Pulse = 1999; //pakeiciamas pulse (sviesos lygis)
				sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
				sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
				HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1); //issaugomi duomenys
				HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1); //paleidziamas pwm (ijungia LED) (reikia sios eilutes kaskart pakeitus pulse)
			  	char *str = " LED ijungta\n"; //tekstas
			  	HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY); //tekstas
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
			}
			else if (strcmp((const char *)Rx_byte, (const char *)LEDOFF) == 0)
			{
				HAL_TIM_Base_Stop_IT(&htim2); //sustabdo timer interrupta
				HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1); //sustabdo pwm (isjungia LED)
			  	char *str = " LED isjungta\n";
			  	HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
			}
			else if (strcmp((const char *)Rx_byte, (const char *)BLINK) == 0)
			{
				HAL_TIM_Base_Stop_IT(&htim2); //sustabdo timer interrupta
				char *str = " LED mirgsejimo kas sekunde rezimas ijungtas\n";
				HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
				timeron = 1; // si funkcija naudojama, kad islipti is while ciklo
				while (timeron == 1)
				{
					if ((strcmp((const char *)Rx_byte, (const char *)LEDON) == 0) || (strcmp((const char *)Rx_byte, (const char *)LEDOFF) == 0) || (strcmp((const char *)Rx_byte, (const char *)BUTTON) == 0) || (strcmp((const char *)Rx_byte, (const char *)PULSE) == 0) || (strcmp((const char *)Rx_byte, (const char *)PWM) == 0))
					{
						timeron = 0;
					}
					else
					{
						TIM_OC_InitTypeDef sConfigOC;
						HAL_TIM_PWM_Init(&htim2);
						sConfigOC.OCMode = TIM_OCMODE_PWM1;
						sConfigOC.Pulse = 1999;
						sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
						sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
						HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);
						HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
						HAL_Delay(1000); //1000ms=1s pauze
						HAL_TIM_PWM_Init(&htim2);
						sConfigOC.OCMode = TIM_OCMODE_PWM1;
						sConfigOC.Pulse = 0;
						sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
						sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
						HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1);
						HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
						HAL_Delay(1000); //1000ms=1s pauze
					}
				}
			}
			else if (strcmp((const char *)Rx_byte, (const char *)BUTTON) == 0)
			{
				HAL_TIM_Base_Stop_IT(&htim2);
				if (HAL_GPIO_ReadPin(B1_button_GPIO_Port, B1_button_Pin) == 0) //tikrina ar mygtukas paspaustas
				{
					char *str = " Mygtukas paspaustas\n";
					HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
				}
				else
				{
					char *str = " Mygtukas nepaspaustas\n";
					HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
				}
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
			}
			else if (strcmp((const char *)Rx_byte, (const char *)PWM) == 0)
			{
				HAL_TIM_Base_Stop_IT(&htim2);
				char *str2 = " Sviesos lygio keitimo rezimas\n";
				HAL_UART_Transmit(&huart2, (uint8_t *)str2, strlen (str2), HAL_MAX_DELAY);
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
				timeron = 1;
				while (timeron == 1)
				{
					if ((strcmp((const char *)Rx_byte, (const char *)LEDON) == 0) || (strcmp((const char *)Rx_byte, (const char *)LEDOFF) == 0) || (strcmp((const char *)Rx_byte, (const char *)BUTTON) == 0) || (strcmp((const char *)Rx_byte, (const char *)PULSE) == 0) || (strcmp((const char *)Rx_byte, (const char *)BLINK) == 0))
					{
						timeron = 0;
					}
					if (HAL_GPIO_ReadPin(B1_button_GPIO_Port, B1_button_Pin) == 0) //tikrina ar mygtukas paspaustas
					{
						HAL_Delay(100); //idedamas delay nes paspaudus mygtuka labai greit nuskaitomi duomenys ir gali pasikeisti duomenys kelis kartus
						if(pwm_value == 0) step = 500;
						if(pwm_value == 2000) step = -500;
						pwm_value += step;
						user_pwm_setvalue_button(pwm_value);
						sprintf(str, "Sviesos lygis = %d \n",pwm_value);
						HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
						HAL_Delay(100);
					}

				}
			}
			else if (strcmp((const char *)Rx_byte, (const char *)PULSE) == 0)
			{
				HAL_TIM_Base_Stop_IT(&htim2);
				char *str = " LED pulsavimo rezimas ijungtas\n";
				HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
				timeron = 1;
				while (timeron == 1)
				{
					if ((strcmp((const char *)Rx_byte, (const char *)LEDON) == 0) || (strcmp((const char *)Rx_byte, (const char *)LEDOFF) == 0) || (strcmp((const char *)Rx_byte, (const char *)BUTTON) == 0) || (strcmp((const char *)Rx_byte, (const char *)PWM) == 0) || (strcmp((const char *)Rx_byte, (const char *)BLINK) == 0))
					{
						timeron = 0;
					}
					HAL_Delay(50);
					//priskaiciavus delay, kad iki max pulse (2000) pakiltu per sekunde
					//20(ms)*20(steps) == 400ms 50(ms)*20(steps) == 1000ms
					//pakyla per sekunde ir dar per sekunde nusileidzia iki 0 tai reiktu 25ms delay kad pakiltu ir nusileistu per sekunde
					if(pwm_value == 0) step = 100;
					if(pwm_value == 2000) step = -100;
					pwm_value += step;
					user_pwm_setvalue(pwm_value);
				}
			}
			//else if ((strcmp((const char *)Rx_byte, (const char *)LEDON) != 0) || (strcmp((const char *)Rx_byte, (const char *)LEDOFF) != 0) || (strcmp((const char *)Rx_byte, (const char *)BUTTON) != 0) || (strcmp((const char *)Rx_byte, (const char *)PWM) != 0) || (strcmp((const char *)Rx_byte, (const char *)BLINK) != 0) || (strcmp((const char *)Rx_byte, (const char *)PULSE) != 0))
			/*else
			{
				char *str = "Komanda neegzistuoja\n";
				HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
			  	memset(Rx_byte, 0, 10); //Isvalo Rx_byte masyvo atminti
			  	Rx_index = 0; //Atstato index i masyvo pradzia
//				xQueueReceive(SimpleQueue, &received, portMAX_DELAY) != pdTRUE;
			}*/
			/*if (Rx_data == 'f')
			{
				char *str = "LED pulsavimo rezimas ijungtas\n";
				HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen (str), HAL_MAX_DELAY);
				HAL_TIM_Base_Start_IT(&htim2);
			}*/ // situ budu galima pulsavimo rezima naudoti su interruptu


		vTaskDelay(TickDelay);
	}
}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART2)
	{
		Rx_byte[Rx_index++] = Rx_data;
		//str3[Rx_index++] = Rx_data;
	}
	HAL_UART_Receive_IT(&huart2, &Rx_data, 1);
	int ToSend = 123456789; // siuncia data i priority queue (PT.1)
	//if (Rx_data=='a' || Rx_data=='b')
	//{
		 /* The xHigherPriorityTaskWoken parameter must be initialized to pdFALSE as
		 it will get set to pdTRUE inside the interrupt safe API function if a
		 context switch is required. */
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;

		if (xQueueSendToFrontFromISR(SimpleQueue, &ToSend, &xHigherPriorityTaskWoken) == pdPASS)
		{
			//char *str3 = "Komanda neegzistuoja\n";
			//HAL_UART_Transmit(&huart2, (uint8_t *)str3, strlen (str3), HAL_MAX_DELAY);
			//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
			//HAL_UART_Transmit(&huart2, (uint8_t *)value, 17, 500);
		}

		/* Pass the xHigherPriorityTaskWoken value into portEND_SWITCHING_ISR(). If
		 xHigherPriorityTaskWoken was set to pdTRUE inside xSemaphoreGiveFromISR()
		 then calling portEND_SWITCHING_ISR() will request a context switch. If
		 xHigherPriorityTaskWoken is still pdFALSE then calling
		 portEND_SWITCHING_ISR() will have no effect */
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	//}
}


/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM4 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */
	/*if (htim == &htim2 )
	{
			HAL_Delay(100);
			if(pwm_value == 0) step = 100;
			if(pwm_value == 2000) step = -100;
			pwm_value += step;
			user_pwm_setvalue(pwm_value);
	}*/ //pulsavimo rezimo interrupt kodas
  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM4) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
